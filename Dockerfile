FROM centos:latest

# having all of the Development Tools is likely overkill, but
# we don't want the CPANM install processes to run into any 
# "gotchas" around the absense of the build tools...
RUN yum groupinstall -y "Development Tools"

# Included so as to have 'netstat' and 'which' (useful when debugging)
RUN yum install -y which net-tools mariadb

RUN yum install -y libxml2-devel curl-devel libyaml-devel
# All of the known parts for Perl and Dancer2
RUN yum install -y perl perl-App-cpanminus

RUN yum install -y "perl(ExtUtils::Embed)"
RUN cpanm Test::More
RUN cpanm Carp
RUN cpanm TAP::Harness::Env
RUN cpanm Module::Build::Tiny
RUN cpanm DateTime
RUN cpanm Number::Format
RUN cpanm Fatal
RUN cpanm XML::SAX
RUN cpanm XML::LibXML XML::Simple
RUN cpanm WWW::Curl::Easy
RUN cpanm Data::Dumper Redis Dancer2::Plugin::Redis Dancer2 Dancer2::Plugin Dancer2::Plugin::Ajax Dancer2::Core::Session Dancer2::Session::Memcached

# expose port 80 for which we'll run "plackup"
EXPOSE 80/TCP
EXPOSE 443/TCP

ENV container docker

WORKDIR "/opt"
RUN git clone https://gitlab.oit.duke.edu/dul-its/crs.git
ENTRYPOINT plackup --port 80 crs/bin/app.psgi
#CMD	["/usr/sbin/httpd", "-D", "BACKGROUND"]
