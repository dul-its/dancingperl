# DancingPerl Docker Image
Provides a Docker image complete with Perl 5 and the (Rails-like) Dancer2 Web Framework. Currently, the experimental image is 
shaping up to be a foundation for deploying the Libraries' [Requests Web Application](https://requests.library.duke.edu) -- a Perl/Dancer2 application.
  
### GitLab Registry Login ###
When using a Linux(-like) CLI, you'll need to log into the "registry" as such:  
  
    docker login gitlab-registry.oit.duke.edu  
  
***Note***: You'll be prompted for your username (*use your NetID*) and password (*use your NetID password*).  
  
### Using the image ###

**To pull**:  
  
    docker pull gitlab-registry.oit.duke.edu/dul-its/dancingperl/image:latest
  

**To run DancingPerl**:  
   
    docker run --name <containername> \
               -p 80:80/tcp \
               gitlab-registry.oit.duke.edu/dul-its/dancingperl/image:latest \
               dancingperl
  
### Important Notes ###
When the container starts, the "plackup" webserver will be serving the `master` branch of the Requests web application on its port 80.  
   
